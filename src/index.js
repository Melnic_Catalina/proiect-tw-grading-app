import 'bootstrap/dist/css/bootstrap.min.css';

import "./theme/argon-design-system-react-master/src/assets/vendor/nucleo/css/nucleo.css";
import "./theme/argon-design-system-react-master/src/assets/vendor/font-awesome/css/font-awesome.min.css";
import "./theme/argon-design-system-react-master/src/assets/scss/argon-design-system-react.scss";


import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

import Home from './components/Layout/Home';
import ProjectList from './components/Project/ProjectList'
import Register from './components/User/Register'
import Login from './components/User/Login'
import Profile from './components/User/Profile'
import NewProject from './components/Project/NewProject'

const routing = (
    <Router>
    <div>
      <Route path="/" component={Home} />
      <Route path="/projects" component={ProjectList} />
      <Route path="/register" component={Register} />
      <Route path="/login" component={Login} />
      <Route path="/profile" component={Profile} />
      <Route path="/project/new" component={NewProject} />
    </div>
  </Router>
)


ReactDOM.render(routing, document.getElementById('root'));
