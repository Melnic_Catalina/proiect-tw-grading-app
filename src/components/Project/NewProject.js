import React from 'react';
import { Tabs, Tab } from 'react-bootstrap'
import { Card, CardBody, CardTitle, Form } from 'reactstrap'
import AddProject from './AddProject'
import AddTeam from './AddTeam'
import AddUsers from './AddUsers'
import AddDeliverable from './AddDeliverable'

class NewProject extends React.Component {
    constructor(props) {
        super(props)

        let userId = sessionStorage.getItem('userId');

        this.state = {
            userID: userId,
            projectID: '',
            teamID: '',
            active: 'project'
        }
    }

    setKey = (k) => {
        this.setState({ active: k })
    }
    setProjectID = (id) => {
        this.setState({ projectID: id })
    }
    setTeamID = (id) => {
        this.setState({ teamID: id })
    }
    render() {
        return (
            <div className="container">
                <Card className=" text-center" style={{backgroundColor: "#f5f7f9"}}>
                    <CardBody>
                        <CardTitle><i className="ni ni-chart-bar-32 mr-1"></i>Create project</CardTitle>
                            <Tabs id="controlled-tab-example" activeKey={this.state.active} onSelect={k=> this.setKey(k)}>
                                <Tab eventKey="project" title="Project">
                                    <AddProject setActive = {this.setKey} userid={this.state.userID} setProjectID={this.setProjectID}/>                                    
                                </Tab>
                                <Tab eventKey="team"  title="Team">
                                    <AddTeam setActive = {this.setKey} userid={this.state.userID} projectid={this.state.projectID} setTeamID={this.setTeamID}/>                                    
                                </Tab>
                                <Tab eventKey="users" title="Teammates" >
                                    <AddUsers userid={this.state.userID} projectid={this.state.projectID} teamid={this.state.teamID}/>
                                </Tab>
                                <Tab eventKey="deliverables" title="Deliverables">
                                    <AddDeliverable  userid={this.state.userID} projectid={this.state.projectID}/>
                                </Tab>
                            </Tabs>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default NewProject;
