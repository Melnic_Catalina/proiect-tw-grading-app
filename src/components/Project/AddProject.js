import React from 'react';
import {
    Card,
    CardBody,
    Button,
    FormGroup,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Form,
    Input,
    Row,
    Col,
}
from 'reactstrap';
import axios from 'axios'
import ReactDatetime from "react-datetime";

class AddProject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            deadline: '',
            link: '',
            userId: '',
            errors: {
                name: '',
                deadline: ''
            }
        }

        const validateForm = (errors) => {
            let valid = true;
            Object.values(errors).forEach(
                // if we have an error string set valid to false
                (val) => val.length > 0 && (valid = false)
            );
            if (this.state.name == '' || this.state.deadline == '')
                valid = false;
            return valid;
        }

        this.handleDateChange = date => {
        
            const valueOfInput = date.format();
            const formattedDate = new Date(valueOfInput);
            this.setState({ deadline: formattedDate })
        };

        this.handleChange = (event) => {
            event.preventDefault();
            const { name, value } = event.target;
            let errors = this.state.errors;

            switch (name) {
                case 'name':
                    errors.name =
                        value.length < 5 ?
                        'Project name must be at least 5 characters !' :
                        '';
                    break;
                case 'deadline':
                    errors.deadline =
                        value.length == 0 ?
                        'Set a deadline!' :
                        '';
                    break;
                default:
                    break;
            }
            this.setState({ errors, [name]: value }, () => {
                // console.log(errors)
            })
        }
        this.handleClick = () => {
            let self = this;
            if (validateForm(this.state.errors)) {
                axios.post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/projects/${this.state.userId}/add`, this.state)
                    .then(function(response) {
                        self.props.setActive('team');
                        self.props.setProjectID(response.data.id)
                    })
                    .catch(function(object) {
                        console.log(object);
                    });
            }
        }
    }

    componentDidMount() {
        console.log(this.props)
        this.setState({ userId: this.props.userid })
    }

    render() {
        const { errors } = this.state;
        return (
            <div>
          <Card className=" text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <Form>
                    <small className="text-uppercase font-weight-bold">Project name</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-id" placeholder="Amazing Project Name" type="text" name="name" onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    {errors.name.length > 0 && <p className='text-danger' style={{fontSize:"14px"}}>{errors.name}</p>}
                    <small className="text-uppercase font-weight-bold">Deadline</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                             <FormGroup>
                                  <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                      <InputGroupText>
                                        <i className="ni ni-calendar-grid-58" />
                                      </InputGroupText>
                                    </InputGroupAddon>
                                    <ReactDatetime
                                    onChange={this.handleDateChange}
                                      inputProps={{
                                        placeholder: "",
                                        input: "date"
                                      }}
                                      timeFormat={false}
                                      closeOnSelect={true}
                                    />
                                  </InputGroup>
                                </FormGroup>
                            </Col>
                        </Row>
                         {errors.deadline.length > 0 && <p className='text-danger' style={{fontSize:"14px"}}>{errors.deadline}</p>}
                    </Form>
                    <Button color="primary" onClick={this.handleClick}>Create</Button>
                </CardBody>
            </Card>
        </div>
        )
    }

}

export default AddProject;
