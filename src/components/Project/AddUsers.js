import React from 'react';
import {
    Card,
    CardBody,
    Button,
    FormGroup,
    Form,
    Row,
    Col,
    Table
}
from 'reactstrap';
import axios from 'axios'
import Select from 'react-select';

class AddUsers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedUserId: '',
            teammates: [],
            users: []
        }

        this.handleClick = () => {
            let userId = this.state.selectedUserId;
            let projectId = this.props.projectid;
            let self = this;
            if (userId != '')
                axios.put(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/teams/${userId}/${projectId}/add`, { id: this.props.teamid })
                .then(function(response) {
                    console.log(response);
                    self.getUsersInfo();
                })
                .catch(function(object) {
                    console.log(object);
                });
        }
        this.handleChange = (opt) => {
            // opt.label, opt.value
            this.setState({
                selectedUserId: opt.value
            })
        }
    }

    componentDidMount() {
        this.getUsersInfo()

    }

    getUsersInfo() {
        let teamId = this.props.teamid;
        let self = this;
        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/users?teamId=${teamId}`, {})
            .then(function(response) {
                let teammates = [];
                response.data.forEach((x) => {
                    let user = {};
                    user.id = x.id;
                    user.name = x.name;
                    user.email = x.email;
                    teammates.push(user);
                })

                self.setState({
                    teammates: teammates
                })
            })
            .catch(function(error) {
                console.log(error);
            });

        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/users?solo=yes`, {})
            .then(function(response) {
                let users = [];
                response.data.forEach((x) => {
                    let user = {};
                    user.value = x.id;
                    user.label = x.email;
                    users.push(user);
                })
                console.log(users)
                self.setState({
                    users: users
                })
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    renderTableData() {
        return this.state.teammates.map((user, index) => {
            const { id, name, email } = user;
            if (name != "")
                return (
                    <tr key={id}>
                       <td>{id}</td>
                       <td>{name}</td>
                        <td>{email}</td>
                    </tr>
                )
            else {
                return (
                    <tr key="0">
                    <td>No user added yet...</td>
                </tr>
                )
            }
        })
    }

    render() {
        return (
            <div>
          <Card className="text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <Form>
                    <small className="text-uppercase font-weight-bold">Search users</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                            <FormGroup>
                             <div className="container">
                              <Select options={this.state.users}
                              onChange={this.handleChange}/>
                            </div>
                             </FormGroup>
                        </Col>
                    </Row>
                    </Form>
                    <Button color="primary" onClick={this.handleClick}>Add user</Button>
                    <Row className="justify-content-center mt-4">
                        <strong className="text-uppercase font-weight-bold">Your team:</strong>
                    </Row>
                    <Table className="mt-3">
                        <thead>
                            <tr>                                
                                <th>ID</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                            </tr>
                        </thead> 
                            <tbody >
                                {this.renderTableData()}
                        </tbody> 
                    </Table>
                </CardBody>
            </Card>
        </div>
        )
    }
}

export default AddUsers;
