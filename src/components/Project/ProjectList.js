import React, { Component } from 'react'
import axios from 'axios'
import { Table } from 'reactstrap'
import Paginations from '../Layout/Paginations'


class ProjectList extends Component {

    constructor(props) {
        super(props)
        let userId = sessionStorage.getItem('userId');
        console.log(userId)
        this.state = {
            userId: userId,
            projects: [{ id: 1, name: "DefaultName", grade: "10", team: "10", deadline: '' }],
            header: [ "id", "name", "grade", "team", "deadline"],
        }
    }

    getData(data) {
        // setTimeout(() => {
        console.log('Our data is fetched');
        var projects = [];

        if (data.length == 0)
            projects = [{ id: 1, name: "DefaultName", grade: "10", team: "DefaultName", deadline: '' }];
        else {
            data.forEach((x) => {
                x.Projects.forEach((y) => {
                    let project = {};
                    project.id = y.id;
                    project.name = y.name;
                    project.grade = y.grade;
                    project.team = x.name;
                    project.deadline = y.deadline;
                    projects.push(project);
                })
            })
        }
        this.setState({
            projects: projects
        })
    }

    FormatTime(time, prefix = "") {
        var date = new Date(time);
        // console.log(date) // returns NaN if it can't parse
        return Number.isNaN(date) ? "" : prefix + date.toLocaleDateString();
    }

    componentDidMount() {
        var self = this;
        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/projects/${this.state.userId}`, {})
            .then(function(response) {
                self.getData(response.data)
            })
            .catch(function(error) {
                console.log(error);
            });
        console.log("Comp mounted~")

    }
    
    renderTableData() {
        return this.state.projects.map((project, index) => {
            const { id, name, grade, team, deadline } = project;
            if (name != "DefaultName")
                return (
                <tr key={id}>
                   <td>{id}</td>
                   <td>{name}</td>
                   <td>{grade}</td>
                   <td>{team}</td>
                   <td>{this.FormatTime(deadline)}</td>
                </tr>
                )
            else {
                return (
                    <tr key="0">
                    <td>No projects found yet...</td>
                </tr>
                )
            }
        })
    }
    renderTableHeader() {
        let header = this.state.header;
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }
    render() {
        return (
            <div className="container h-100">
            <div className="row h-100 justify-content-center align-items-center">
            <Table>
                <thead>
                    <tr>{this.renderTableHeader()}</tr>
                </thead> 
                    <tbody >
                        { this.renderTableData() } 
                    </tbody> 
            </Table>
            </div>
             <div className="row h-100 justify-content-center align-items-center"><Paginations/></div> 
            </div>
        );
    }
}

export default ProjectList;
