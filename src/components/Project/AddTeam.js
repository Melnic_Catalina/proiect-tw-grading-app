import React from 'react';
import {
    Card,
    CardBody,
    Button,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
}
from 'reactstrap';
import axios from 'axios'

class AddTeam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            errors: {
                name: ''
            }
        }

        const validateForm = (errors) => {
            let valid = true;
            Object.values(errors).forEach(
                // if we have an error string set valid to false
                (val) => val.length > 0 && (valid = false)
            );
            if (this.state.name == '')
                valid = false;
            return valid;
        }


        this.handleChange = (event) => {
            event.preventDefault();
            const { name, value } = event.target;
            let errors = this.state.errors;

            switch (name) {
                case 'name':
                    errors.name =
                        value.length < 5 ?
                        'Team name must be at least 5 characters !' :
                        '';
                    break;
                default:
                    break;
            }
            this.setState({ errors, [name]: value }, () => {
                // console.log(errors)
            })
        }
        this.handleClick = () => {
            let projectId = this.props.projectid;
            let userId = this.props.userid;

            let self = this;
            if (validateForm(this.state.errors)) {
                axios.post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/teams/${projectId}/${userId}/create`, this.state)
                    .then(function(response) {
                        console.log(response);
                        self.props.setActive('users');
                        self.props.setTeamID(response.data.id);
                    })
                    .catch(function(object) {
                        console.log(object);
                    });
            }
        }
    }


    render() {
        const { errors } = this.state;
        return (
            <div>
          <Card className=" text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <Form>
                    <small className="text-uppercase font-weight-bold">Team name</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-id" placeholder="Amazing Team Name" type="text" name="name" onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    {errors.name.length > 0 && <p className='text-danger' style={{fontSize:"14px"}}>{errors.name}</p>}
                    </Form>
                    <Button color="primary" onClick={this.handleClick}>Create team</Button>
                </CardBody>
            </Card>
        </div>
        )
    }

}

export default AddTeam;
