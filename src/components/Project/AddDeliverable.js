import React from 'react';
import {
    Card,
    CardBody,
    Button,
    FormGroup,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Form,
    Input,
    Row,
    Col,
    Table,
    UncontrolledTooltip
}
from 'reactstrap';
import axios from 'axios'
import ReactDatetime from "react-datetime";

class AddDeliverable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            deadline: '',
            deliverables: [],
            errors: {
                name: '',
                deadline: ''
            }
        }

        const validateForm = (errors) => {
            let valid = true;
            Object.values(errors).forEach(
                // if we have an error string set valid to false
                (val) => val.length > 0 && (valid = false)
            );
            if (this.state.name == '' || this.state.deadline == '')
                valid = false;
            return valid;
        }

        this.handleDateChange = date => {
            const valueOfInput = date.format();
            const formattedDate = new Date(valueOfInput);
            this.setState({ deadline: formattedDate })
        };

        this.handleChange = (event) => {
            event.preventDefault();
            const { name, value } = event.target;
            let errors = this.state.errors;

            switch (name) {
                case 'name':
                    errors.name =
                        value.length < 5 ?
                        'Deliverable name must be at least 5 characters !' :
                        '';
                    break;
                case 'deadline':
                    errors.deadline =
                        value.length == 0 ?
                        'Set a deadline!' :
                        '';
                    break;
                default:
                    break;
            }
            this.setState({ errors, [name]: value }, () => {
                // console.log(errors)
            })
        }
        this.handleClick = () => {
            let projectId = this.props.projectid;
            let self = this;
            if (validateForm(this.state.errors)) {
                axios.post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/deliverables/add?projectId=${projectId}`, this.state)
                    .then(function(response) {
                        self.getDeliverablesInfo();
                        this.state.name = '';
                    })
                    .catch(function(object) {
                        console.log(object);
                    });
            }
        }
        this.handleDelete = (event) => {
            let deliverableid = event.target.dataset.deliverableid;
            let self = this;
            axios.delete(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/deliverables/delete/${deliverableid}`, this.state)
                .then(function(response) {
                    self.getDeliverablesInfo();
                })
                .catch(function(object) {
                    console.log(object);
                });
        }
    }

    componentDidMount() {
        this.getDeliverablesInfo()

    }

    FormatTime(time, prefix = "") {
        var date = new Date(time);
        console.log(date) // returns NaN if it can't parse
        return Number.isNaN(date) ? "" : prefix + date.toLocaleDateString();
    }

    getDeliverablesInfo() {
        let projectId = this.props.projectid;
        let self = this;
        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/deliverables/${projectId}`, {})
            .then(function(response) {
                let deliverables = [];
                response.data.forEach((x) => {
                    let deliverable = {};
                    deliverable.id = x.id;
                    deliverable.name = x.name;
                    deliverable.deadline = self.FormatTime(x.deadline);
                    deliverables.push(deliverable);
                })

                self.setState({
                    deliverables: deliverables
                })
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    renderTableData() {
        return this.state.deliverables.map((deliverable, index) => {
            const { id, name, deadline } = deliverable;
            return (
                <tr key={id}>
                       <td>{id}</td>
                       <td>{name}</td>
                       <td>{deadline}</td>
                       <td>
                            <Button
                              color="danger"
                              data-placement="right"
                              id="tooltip611234743"
                              size="sm"
                              type="button"
                              data-deliverableid={id} 
                              onClick={this.handleDelete}
                            >
                             <span className="btn-inner--icon">
                                <i className="ni ni-fat-remove" />
                              </span>
                            </Button>
                        <UncontrolledTooltip
                          delay={0}
                          placement="right"
                          target="tooltip611234743"
                        >
                        Delete
                    </UncontrolledTooltip>      
                       </td>
                    </tr>
            )
        })
    }

    render() {
        const { errors } = this.state;
        return (
            <div>
          <Card className=" text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <Form>
                    <small className="text-uppercase font-weight-bold">Deliverable name</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-id" placeholder="Amazing Deliverable Name" type="text" name="name" onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    {errors.name.length > 0 && <p className='text-danger' style={{fontSize:"14px"}}>{errors.name}</p>}
                    <small className="text-uppercase font-weight-bold">Deliverable deadline</small>
                        <Row className="justify-content-center mt-2">
                            <Col md="4">
                             <FormGroup>
                                  <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                      <InputGroupText>
                                        <i className="ni ni-calendar-grid-58" />
                                      </InputGroupText>
                                    </InputGroupAddon>
                                    <ReactDatetime
                                    onChange={this.handleDateChange}
                                      inputProps={{
                                        placeholder: "",
                                        input: "date"
                                      }}
                                      timeFormat={false}
                                      closeOnSelect={true}
                                    />
                                  </InputGroup>
                                </FormGroup>
                            </Col>
                        </Row>
                         {errors.deadline.length > 0 && <p className='text-danger' style={{fontSize:"14px"}}>{errors.deadline}</p>}
                    <Button color="primary" onClick={this.handleClick}>Add</Button>
                    <Row className="justify-content-center mt-4">
                        <strong className="text-uppercase font-weight-bold">Project deliverables:</strong>
                    </Row>
                    <Table className="mt-3">
                        <thead>
                            <tr>                                
                                <th>ID</th>
                                <th>NAME</th>
                                <th>DEADLINE</th>
                            </tr>
                        </thead> 
                            <tbody >
                                {this.renderTableData()}
                        </tbody> 
                    </Table>
                    </Form>
                </CardBody>
            </Card>
        </div>
        )
    }

}

export default AddDeliverable;
