import React, { Component } from 'react';
import axios from 'axios'
import {
    Card,
    CardBody,
    CardTitle,
    Button,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
    UncontrolledAlert
}
from 'reactstrap';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            isFailed: false
        }

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.handleClick = (evt) => {
            let self = this;
            axios.post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/login`, this.state)
                .then(function(response) {
                    sessionStorage.setItem('userId', response.data.id);

                    self.props.history.push('/projects');
                })
                .catch(function(error) {
                    console.log(error);
                    self.setState({ isFailed: true });
                    setTimeout(() => {
                        self.setState({ isFailed: false });
                    }, 3000)
                });

        }
        this.renderAlert = () => {
            if (this.state.isFailed == true) {
                return (
                    <Row className="justify-content-center mt-3">
                    <Col md="5">
                    <UncontrolledAlert color="danger" fade={false}>
                      <span className="alert-inner--icon">
                        <i className="ni ni-like-2" />
                      </span>{" "}
                      <span className="alert-inner--text">
                        <strong>Failed!</strong> Account not found!
                      </span>
                </UncontrolledAlert>
                    </Col>
                    </Row>
                )
            }
        }
    }
    render() {
        return (
            <div className="container">
            {this.renderAlert()}
            <Card className=" text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <CardTitle>Sign in</CardTitle>
                    <Form>
                        <Row className="justify-content-center">
                            <Col md="4" >
                                <FormGroup>
                                    <Input id="input-id" placeholder="name@example.com" type="email" name="email" onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col md="4">
                                <FormGroup>
                                    <Input  id="input-name"placeholder="password" type="password" name="password" onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                       </Row>
                    </Form>
                    <Button color="primary" onClick={this.handleClick}>Login</Button>
                </CardBody>
            </Card>
        </div>
        )
    }
}

export default Login;
