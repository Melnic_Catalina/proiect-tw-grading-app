import React, { Component } from 'react';
import axios from 'axios'
import {
    Card,
    CardBody,
    CardTitle,
    Button,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
    UncontrolledAlert,
    UncontrolledTooltip
}
from 'reactstrap';

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            name: '',
            password: '',
            type: '',
            alerts: {
                title: '',
                type: '',
                message: ''
            },
            errors: {
                name: '',
                email: '',
                password: '',
                type: ''
            }

        }

        const validEmailRegex =
            RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        const validateForm = (errors) => {
            let valid = true;
            Object.values(errors).forEach(
                // if we have an error string set valid to false
                (val) => val.length > 0 && (valid = false)
            );
            if (this.state.name == null || this.state.email == null || this.state.password == null || this.state.type == null)
                valid = false;
            return valid;
        }

        this.handleChange = (event) => {
            event.preventDefault();
            const { name, value } = event.target;
            let errors = this.state.errors;

            switch (name) {
                case 'name':
                    errors.name =
                        value.length < 5 ?
                        'Name must be 5 characters long!' :
                        '';
                    break;
                case 'email':
                    errors.email =
                        validEmailRegex.test(value) ?
                        '' :
                        'Email is not valid!';
                    break;
                case 'password':
                    errors.password =
                        value.length < 8 ?
                        'Password must be 8 characters long!' :
                        '';
                    break;
                case 'type':
                    errors.type =
                        value.length == 0 ?
                        'Choose student or teacher!' :
                        '';
                    break;
                default:
                    break;
            }
            this.setState({ errors, [name]: value }, () => {
                // console.log(errors)
            })
        }

        this.handleClick = (evt) => {
            console.log(this.state)
            let self = this;
            let userId = sessionStorage.getItem('userId');
            if (validateForm(this.state.errors)) {
                axios.put(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/updateUser/${userId}`, this.state)
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
            else {

            }
        }

        this.renderAlert = () => {
            if (this.state.alerts.title != '') {
                return (
                    <Row className="justify-content-center mt-3">
                    <Col md="5">
                    <UncontrolledAlert color={this.state.alerts.type} fade={false}>
                      <span className="alert-inner--icon">
                        <i className="ni ni-like-2" />
                      </span>{" "}
                      <span className="alert-inner--text">
                        <strong>{this.state.alerts.title}</strong> {this.state.alerts.message}
                      </span>
                </UncontrolledAlert>

                    </Col>
                    </Row>
                )
            }
        }
    }
    componentDidMount() {
        let userId = sessionStorage.getItem('userId');
        let self = this;
        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/profile/${userId}`, {})
            .then(function(response) {
                console.log(response.data)
                self.setState({ name: response.data.name, password: response.data.password, email: response.data.email, type: response.data.type })
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        const { errors } = this.state;
        return (
            <div className="container">
            {this.renderAlert()}
            <Card className=" text-center" style={{backgroundColor:"#f5f7f9"}}>
                <CardBody>
                    <CardTitle><i className="ni ni-circle-08 mr-1"></i>Update user</CardTitle>
                        <Button
                          color="info"
                          data-placement="bottom"
                          id="tooltip611234743"
                          size="sm"
                          type="button"
                        >
                          Account type
                        </Button>
                    <UncontrolledTooltip
                          delay={0}
                          placement="bottom"
                          target="tooltip611234743"
                        >
            {this.state.type.toUpperCase()}
            </UncontrolledTooltip>
                    <p className="text-primary"></p>
                    <Form>
                        <Row className="justify-content-center">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-id" placeholder="name@example.com" type="email" name="email" onChange={this.handleChange} value={this.state.email}/>
                                </FormGroup>
                            </Col>
                        </Row>
                        {errors.email.length > 0 && <p className='text-danger' style={{fontSize:"15px"}}>{errors.email}</p>}
                        <Row className="justify-content-center">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-name" placeholder="name" type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        {errors.name.length > 0 && <p className='text-danger' style={{fontSize:"15px"}}>{errors.name}</p>}
                        <Row className="justify-content-center">
                            <Col md="4">
                                <FormGroup>
                                    <Input id="input-name" placeholder="password" type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        {errors.password.length > 0 && <p className='text-danger' style={{fontSize:"15px"}}>{errors.password}</p>}
                    </Form>
                    <Button color="primary" onClick={this.handleClick}>Save</Button>
                </CardBody>
            </Card>
        </div>
        )
    }
}

export default Profile;
