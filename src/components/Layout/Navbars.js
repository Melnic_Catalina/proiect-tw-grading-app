import React, { useRef } from "react";
import { Link, Redirect } from "react-router-dom";
// reactstrap components
import {
  UncontrolledCollapse,
  Navbar,
  NavbarBrand,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col
}
from "reactstrap";

const LoggedInOptions = (props) => {
  const handleClick = (evt) => {
    if (evt.target.name == "logout") {
      sessionStorage.removeItem('userId');
    }
    props.history.push(evt.target.dataset.to)
  }
  return (
    <Nav className="ml-lg-auto" navbar>
        <NavItem>
          <NavLink data-to="/projects" onClick={handleClick}>
              Your projects
            </NavLink>
          </NavItem>
          <NavItem>
          <NavLink data-to="/project/new" onClick={handleClick}>
              New project 
            </NavLink>
          </NavItem>
           <NavItem>
            <NavLink data-to="/profile" onClick={handleClick}>
              Profile
            </NavLink>
          </NavItem>
               <NavItem>
           <NavLink  data-to="/login" name="logout" onClick={handleClick}>
              Sign out
            </NavLink>
          </NavItem>
      </Nav>
  );
}

const LoggedOutOptions = (props) => {
  const handleClick = (evt) => {
    props.history.push(evt.target.dataset.to)
  }
  return (
    <Nav className="ml-lg-auto" navbar>
            <NavItem>
                <NavLink data-to="/register" onClick={handleClick}>
                    Register
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink data-to="/login" onClick={handleClick}>
                    Sign in
                </NavLink>
            </NavItem>
      </Nav>
  )
}
class Navbars extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: null
    }
  }


  componentDidMount() {
    // setez interval de rerender pentru meniu verificand daca user este logat sau nu
    setInterval(() => {
      let userId = sessionStorage.getItem('userId');
      this.setState({ userId: userId });
    }, 100)
  }
  render() {
    let navOptions;

    if (this.state.userId) {
      navOptions = <LoggedInOptions history={this.props.history}/>;
    }
    else {
      navOptions = <LoggedOutOptions history={this.props.history}/>;
    }
    return (
      <div>
        <Navbar className="navbar-horizontal navbar-dark bg-primary" expand="lg">
            <Container>
                <NavbarBrand href="#pablo" onClick={e=> {e.preventDefault()}}> Grade us
                </NavbarBrand>
                <button aria-controls="navbar-primary" aria-expanded={false} aria-label="Toggle navigation" className="navbar-toggler" data-target="#navbar-primary" data-toggle="collapse" id="navbar-primary" type="button">
                    <span className="navbar-toggler-icon" />
                </button>
                <UncontrolledCollapse navbar toggler="#navbar-primary">
                    <div className="navbar-collapse-header">
                        <Row>
                            <Col className="collapse-close" xs="6">
                                <button aria-controls="navbar-primary" aria-expanded={false} aria-label="Toggle navigation" className="navbar-toggler" data-target="#navbar-primary" data-toggle="collapse" id="navbar-primary" type="button">
                                    <span />
                                    <span />
                                </button>
                            </Col>
                        </Row>
                    </div>
                        {navOptions}
                </UncontrolledCollapse>
            </Container>
        </Navbar>
      </div>
    );
  }
}

export default Navbars;
