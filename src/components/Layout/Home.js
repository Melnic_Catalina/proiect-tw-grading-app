import React, { Component } from 'react';
import Navbars from './Navbars'

class Home extends Component {
  render() {
    return (
      <div>
        <div><Navbars history={this.props.history}/></div>
      </div>
    )
  }
}

export default Home;
