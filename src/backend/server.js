const express = require('express')
const bodyParser = require('body-parser')
const { sequelize, Project, User, Team, Deliverable, Jury, Grade } = require('./models');
const cors = require('cors');
const Sequelize = require("sequelize");
const moment = require("moment")

const app = express();
app.use(bodyParser.json());
app.use(
    cors({
        origin: `http://13.58.189.178:8080`,
        credentials: true,
    })
);

app.locals.projects = [];

app.get('/', (request, response) => {
    console.log("test")
    response.status(200).send("This is the homepage! Nothing to do here.");
});
//OLD -------------------------------------
{
    // app.get('/projects', (request, response) => {
    //     response.status(200).send(app.locals.projects);
    // });

    // app.get('/projects/:name', (request, response) => {
    //     const name = request.params.name;
    //     console.log(name);
    //     var i = 0;
    //     var varProject = {};
    //     for (; i < app.locals.projects.length; i++) {
    //         if (app.locals.projects[i].name == name) {
    //             varProject = app.locals.projects[i];
    //         }
    //     }
    //     if (varProject.name == name)
    //         response.status(200).send(varProject);
    //     else
    //         response.status(404).send({ message: "Cannot save project!" });
    // })

    // app.post('/add', (request, response) => {
    //     const project = request.body;
    //     if (project.name && project.number) {
    //         app.locals.projects.push(project);
    //         response.status(200).send("Project added.");
    //     }
    //     else {
    //         response.status(500).send("Cannot add project.");
    //     }
    // });
}
//OLD--------------------------------------

// metoda pentru login primeste un user
// si verifica existenta lui in baza
app.post('/login', (request, response) => {
    const user = request.body;
    if (user.email && user.password) {
        User.findOne({ where: { email: user.email, password: user.password } }).then(result => {
            if (result) {
                response.status(200).send({ id: result.id, message: "Login successfull!" });
            }
            else {
                response.status(404).send("Login failed!");
            }
        })
    }
});

// metoda ce intoarce datele aferente userului pentru actualizarea profilului
app.get('/profile/:userId', (request, response) => {
    const userId = request.params.userId;

    User.findOne({ where: { id: userId } }).then((foundUser) => {
        response.status(200).send(foundUser);
    })
})
// metoda pentru register primeste un user
// si verifica existenta lui in baza
// daca nu exista il introduce in baza
app.post('/register', (request, response) => {
    const user = request.body;
    if (user.name && user.email && user.password && user.type) {
        User.findOne({ where: { email: user.email } }).then(result => {
            if (result) {
                response.status(409).send({
                    message: "The email already exists!"
                })
            }
            else {
                User.build(user).save().then(user => {
                    response.status(201).send({
                        message: "Registration successfully!"
                    });
                })
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing user fields.'
        });
    }
})

// metoda pentru update user primeste un user
// si verifica existenta lui in baza
// daca exista modifica datele lui din baza
app.put('/updateUser/:userId', (request, response) => {
    const user = request.body;
    const userId = request.params.userId;

    if (user.name && user.email && user.password) {
        User.findOne({ where: { id: userId } }).then(result => {
            if (result) {
                User.update(user, { where: { email: user.email } }).then(user => {
                    response.status(201).send({
                        message: "User updated successfully"
                    });
                })
            }
            else {
                response.status(404).send({
                    message: "User doesn't exist"
                });
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing user fields'
        });
    }
})

// metoda ce returneaza toate proiectele aferente unui user sau toate proiectele inclusiv notele daca userul este profesor
app.get('/projects/:userId', (request, response) => {
    const userId = request.params.userId;
    User.findOne({ where: { id: userId } }).then(result => {
        if (result) {
            if (result.type == "teacher") {
                Team.findAll({
                    attributes: ['name'],
                    include: [{ model: Project, attributes: ['id', 'name', 'grade', 'deadline'] }]
                }).then(projects => {
                    response.status(200).send(projects);
                });
            }
            else {
                Team.findOne({
                    attributes: ['name'],
                    where: { id: result.TeamId },
                    include: [{ model: Project, attributes: ['id', 'name', 'deadline'] }]
                }).then(projects => {
                    response.status(200).send(projects);
                });
            }
        }
        else {
            response.status(404).send("Can't find user!");
        }
    })

});

// metoda ce intoarce toti userii care nu sunt profesori sau cei care apartin/nu apartin deja unei echipe
app.get('/users', (request, response) => {
    let teamId = request.query.teamId;
    let solo = request.query.solo;

    if (teamId) {
        User.findAll({ attributes: ['name', 'email', 'id'], where: { type: "student", TeamId: teamId } }).then((result) => {
            if (result) {
                response.status(200).send(result);
            }
            else {
                response.status(404).send("Can't find users!");
            }
        })
    }
    else if (solo) {
        User.findAll({ attributes: ['email', 'id'], where: { type: "student", TeamId: null } }).then((result) => {
            if (result) {
                response.status(200).send(result);
            }
            else {
                response.status(404).send("Can't find users!");
            }
        })
    }
})

// metoda pentru adaugare de proiect nou 
// primeste in request body datele aferente
// unui proiect nou
// validand cazul in care se incearca creearea
// unui proiect cu acelasi nume
app.post('/projects/:userId/add', (request, response) => {
    const project = request.body;
    const userId = request.params.userId;

    project.deadline = new Date(project.deadline)
    moment.defaultFormat = "D/M/YYYY";
    project.deadline = moment(project.deadline, moment.defaultFormat).toDate()

    if (project.name && project.deadline) {
        Project.findOne({ where: { name: project.name } }).then(result => {
            if (result) {
                response.status(409).send({
                    message: "A project with the same name already exists"
                })
            }
            else {
                const jury = {};
                Jury.build(jury).save().then(jury => {
                    project.JuryId = jury.id;
                    Project.build(project).save().then(project => {
                        const projectId = project.id;
                        User.update({ JuryId: jury.id }, { where: { id: userId } });
                        Project.update({ JuryId: jury.id }, { where: { id: projectId } }).then(() => {
                            response.status(201).send({
                                message: "Project added successfully",
                                id: projectId
                            });
                        })
                    })
                })
            }

        })
    }
    else {
        response.status(400).send({
            message: 'Missing project fields'
        });
    }
})

// metoda pentru adaugarea unei echipe la proiect
// odata creat proiectul, pentru el este adaugata o echipa
// cel care creeaza echipa este introdus in ea
app.post('/teams/:projectId/:userId/create', (request, response) => {
    const projectId = request.params.projectId;
    const leaderId = request.params.userId;

    const team = request.body;
    if (team.name) {
        Team.findOne({ where: { name: team.name } }).then(result => {
            if (result) {
                response.status(409).send({
                    message: "A team with the same name already exists"
                })
            }
            else {
                Team.build(team).save().then(team => {
                    var teamId = team.id;
                    Project.update({ TeamId: teamId }, { where: { id: projectId } }).then(project => {
                        User.update({ TeamId: teamId }, { where: { id: leaderId } }).then(user => {
                            response.status(201).send({
                                message: "Team added to project successfully",
                                id: teamId
                            });
                        })

                    })
                })
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing team fields'
        });
    }
})

// metoda pentru adaugare de membru in echipa deja creata
// primeste userId ca request param userul care urmeaza
// sa fie adaugat in echipa
app.put('/teams/:userId/:projectId/add', (request, response) => {
    const userId = request.params.userId;
    const projectId = request.params.projectId
    const team = request.body;
    if (team.id) {
        Team.findOne({ where: { id: team.id } }).then(result => {
            if (result) {
                const team = result;
                // console.log(result.id)
                Project.findOne({ where: { id: projectId } }).then(project => {
                    User.update({ TeamId: team.id, JuryId: project.JuryId }, { where: { id: userId } }).then(user => {
                        response.status(201).send({
                            message: "User added to team!"
                        });
                    })
                })
            }
            else {
                response.status(404).send({
                    message: "Team not found!"
                });
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing project fields'
        });
    }
})

// metoda pentru adaugare de livrabil nou in proiectul deja creeat
// primeste projectId ca request param proiectul pentru care urmeaza
// sa fie adaugat livrabilul
app.post('/deliverables/add', (request, response) => {
    const projectId = request.query.projectId;
    const deliverable = request.body;
    deliverable.deadline = new Date(deliverable.deadline)
    moment.defaultFormat = "D/M/YYYY";
    deliverable.deadline = moment(deliverable.deadline, moment.defaultFormat).toDate()

    if (deliverable.name && deliverable.deadline) {
        Deliverable.findOne({ where: { name: deliverable.name, projectId: projectId } }).then(result => {
            if (result) {
                response.status(409).send({
                    message: "A deliverable with the same name for this project already exists"
                })
            }
            else {
                deliverable.ProjectId = projectId;
                Deliverable.build(deliverable).save().then(project => {
                    response.status(201).send({
                        message: "Deliverable added to project!"
                    });
                })
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing user fields'
        });
    }
})

// metoda ce intoarce livrabilele aferente unui proiect 
app.get('/deliverables/:projectId', (request, response) => {
    const projectId = request.params.projectId;
    Project.findOne({ where: { id: projectId } }).then(result => {
        if (result) {
            Deliverable.findAll({
                where: { projectId: result.id }
            }).then(result => {
                response.status(200).send(result);
            });
        }
        else {
            response.status(404).send("Can't find any deliverables for this project!");
        }
    })
})

app.delete('/deliverables/delete/:deliverableId', (request, response) => {
    const deliverableId = request.params.deliverableId;
    Deliverable.findOne({ where: { id: deliverableId } }).then((deliverable) => {
        if (deliverable) {
            console.log(deliverable)
            deliverable.destroy().then(() => {
                response.status(204).send({ message: 'Deliverable deleted!' });
            })
        }
        else {
            response.status(404).send({ message: 'Deliverable not found!' });
        }
    })

})

app.post('/projects/:userId/:projectId/addGrade', (request, response) => {
    const projectId = request.params.projectId;
    const userId = request.params.userId;
    const grade = request.body;

    grade.ProjectId = projectId;
    grade.UserId = userId;

    Grade.build(grade).save().then(project => {
        response.status(201).send({
            message: "Anon user graded the project!"
        });
    })
})

// metoda pentru calculare nota finala pentru proiectul specificat,
// primeste projectId ca request param proiectul pentru care urmeaza
// sa fie calculata nota finala
app.put('/projects/:projectId/finalGrade', (request, response) => {
    const projectId = request.params.projectId;
    const finalGrade = 10;

    Project.findOne({ where: { id: projectId } }).then(result => {
        if (result) {
            var project = result;
            Grade.findAll({ attributes: ['grade'], where: { ProjectId: projectId }, raw: true }).then((grades) => {
                console.log(grades);
                var gradesArray = grades.map(function(obj) {
                    return obj.grade;
                });

                // console.log(grades.map(grades, function(grade) { return grade.Name; }));
                var min = Math.min(...(gradesArray)),
                    max = Math.max(...(gradesArray)),
                    sum = gradesArray.reduce((a, b) => parseInt(a) + parseInt(b), 0)


                console.log(min + "-----" + max + "------" + sum);

                var unformattedGrade = (sum - min - max) / (gradesArray.length - 2);

                project.grade = (Math.round(unformattedGrade * 100) / 100).toFixed(2);
                Project.update({ grade: project.grade }, { where: { id: projectId } }).then(project => {
                    response.status(201).send({
                        message: "Project graded!"
                    });
                })
            })
        }
        else {
            response.status(500).send({
                message: 'Grading failed!'
            });
        }
    })
})

app.listen(3001, () => {
    console.log("App listening on port 3001...");
});

// Instructiuni drop table pentru MySql
// SET foreign_key_checks = 0;
// -- Drop tables
// drop table ...
// -- Drop views
// drop view ...
// SET foreign_key_checks = 1;
