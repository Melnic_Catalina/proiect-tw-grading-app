const Sequelize = require('sequelize');
const mysql = require('mysql2/promise')

const DB_USERNAME = 'newUser'
const DB_PASSWORD = 'Welcome123#'

mysql.createConnection({
        user: DB_USERNAME,
        password: DB_PASSWORD
    })
    .then(async(connection) => {
        // 	await connection.query('DROP DATABASE IF EXISTS grading_db')
        await connection.query('CREATE DATABASE IF NOT EXISTS grading_db')
    })
    .catch((err) => {
        console.warn(err.stack)
    })

const sequelize = new Sequelize('grading_db', DB_USERNAME, DB_PASSWORD, {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});
sequelize.authenticate().then(() => {
    console.log('Connection established successfully!');
}).catch(err => {
    console.log(`Error on connection: ${err}`);
});

class User extends Sequelize.Model {}
class Deliverable extends Sequelize.Model {}
class Project extends Sequelize.Model {}
class Team extends Sequelize.Model {}
class Jury extends Sequelize.Model {}
class Grade extends Sequelize.Model {}

User.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    type: Sequelize.STRING
}, {
    sequelize,
    modelName: 'Users'
});

Team.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.STRING
}, {
    sequelize,
    modelName: 'Teams'
});

Jury.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    }
}, {
    sequelize,
    modelName: 'Juries'
});

Project.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    link: Sequelize.STRING,
    name: Sequelize.STRING,
    grade: Sequelize.DECIMAL(4, 2),
    deadline: Sequelize.DATE,
    JuryId: Sequelize.INTEGER
}, {
    sequelize,
    modelName: 'Projects'
});

Deliverable.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.STRING,
    deadline: Sequelize.DATE
}, {
    sequelize,
    modelName: 'Deliverables'
});

Grade.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    grade: Sequelize.DECIMAL(4, 2)
}, {
    sequelize,
    modelName: 'Grades'
});


Project.hasMany(Deliverable);
Team.hasMany(User)
Team.hasMany(Project)
Jury.hasMany(User)
Project.hasMany(Jury)
User.hasMany(Grade)
Project.hasMany(Grade)

// Deliverable.sync();
// Project.sync();
// User.sync();
// Team.sync();
// Jury.sync();
// Grade.sync();

sequelize.sync();

module.exports = {
    sequelize,
    User,
    Project,
    Team,
    Grade,
    Jury,
    Deliverable
}
